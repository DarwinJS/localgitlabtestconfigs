echo
echo "*******************"
echo "CONFIGURATION: Container w/ Persisted Local Data"
echo "PHASE: Starting GitLab Setup"
echo "  => Installing GitLab using docker-compose"

#From: https://medium.com/better-programming/using-a-k3s-kubernetes-cluster-for-your-gitlab-project-b0b035c291a9

if [[ -f "./docker-compose.yml" ]]; then
  echo "     Using docker-compose.yml in this folder, delete or rename it to retrieve the latest (which will reference the latest GitLab version)."
  echo "     Note this docker-compose file is from the community - it is not an official GitLab artifact."
else
  curl -sSL https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml --output docker-compose.yml
fi

if [[ -n "$(command -v docker-machine)" ]]; then
  echo "Making sure docker daemon works through docker-machine"
  eval $(docker-machine env default)
fi

if [[ -n "$(command -v docker-machine)" ]]; then
  echo "IMPORTANT: You may be using Docker via docker machine, in which case docker containers are available at the following LOCAL ip address rather than 127.0.0.1:"
  echo "GitLab UI will be at port 10080 on the below ip address"
  $(docker-machine ls default)
fi

if [[ ! "$(docker-compose ps)" == *"_gitlab_"* ]]; then
  docker-compose up
fi

if [[ ! "$(docker network inspect bridge)" == *"_gitlab_"* ]]; then
  echo "     Wiring GitLab Web container into Docker bridge network where KIND Kubernetes cluster will be wired up to."
  docker network connect bridge localgitlabwithkube_gitlab_1
fi

cat <<"END_OF_FINAL_INSTRUCTIONS"

NOTE: You can force the version of GitLab (and associated containers) by retreiving the docker-compose.yml associated
      with a specific tag in this repo: https://github.com/sameersbn/docker-gitlab/tags

For Mac, Your local ip address will be the ip address of the docker machine used to support docker.
Find it's ip address with this command at the console:
docker-machine ls default

IF YOU WILL BE CONNECTING OTHER LOCALLY INSTALLED SERVICES TO GITLAB (KUBERNETES / JENKINS): 

In GitLab admin, enable local network requests:
Gitlab Admin => Settings => Network => Outbound requests => Allow requests to the local network from web hooks and services (checked)

If you have problems with the GitLab password, use rails to reset it like this
docker exec -it localgitlabwithkube_gitlab_1 bash
su - git
cd gitlab
bundle exec rails c production
user = User.where(id: 1).first
user.password = '5iveL!fe'
user.save
exit

END_OF_FINAL_INSTRUCTIONS
