
echo
echo "*******************"
echo "CONFIGURATION: Jenkins Container w/ Persisted Local Data"
echo "PHASE: Starting Jenkins Setup"
echo "  => Installing Jenkins using docker-compose w/ Binami image"

if [[ -f "./docker-compose.yml" ]]; then
  echo "     Using docker-compose.yml in this folder, delete it to retrieve the latest."
  echo "     Note this docker-compose file is from the community - it is not an official GitLab artifact."
else
  curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-jenkins/master/docker-compose.yml --output docker-compose.yml
fi

if [[ -n "$(command -v docker-machine)" ]]; then
  echo "Making sure docker daemon works through docker-machine"
  eval $(docker-machine env default)
fi

if [[ -n "$(command -v docker-machine)" ]]; then
  echo "IMPORTANT: You may be using Docker via docker machine, in which case docker containers are available at the following LOCAL ip address rather than 127.0.0.1:"
  echo "Jenkins UI will be at port 8080 on the below ip address"
  $(docker-machine ls default)
fi

if [[ ! "$(docker-compose ps)" == *"jenkins_1"* ]]; then
  docker-compose up
fi

cat <<"END_OF_FINAL_INSTRUCTIONS"

NOTE: You can force the version of Jenkins an the distro by retreiving the docker-compose.yml associated with a 
      specific tag in this repo: https://github.com/bitnami/bitnami-docker-jenkins/tags

For Mac, Your local ip address will be the ip address of the docker machine used to support docker.
Find it's ip address with this command at the console:
docker-machine ls default

IF YOU WILL BE REACHING OUT TO THIS JENKINS SERVER FROM A LOCALLY GITLAB INSTANCE, ON THE LOCAL INSTANCE: 

In GitLab admin, enable local network requests:
Gitlab Admin => Settings => Network => Outbound requests => Allow requests to the local network from web hooks and services (checked)

END_OF_FINAL_INSTRUCTIONS
