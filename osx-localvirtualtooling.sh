#!/bin/bash
#Initial Author: Darwin Sanoy
#This initial version is Mac only - autodetection of the platform / package manager may be all
#  that is needed to make it work on Linux as well
MYVERSION=0.3

#Run from GitLab:
    #bash <(wget -qO - https://gitlab.com/DarwinJS/quick-config/raw/master/macconfig.sh) <ARGUMENTS>
    #wget -O - https://gitlab.com/DarwinJS/quick-config/raw/master/macconfig.sh | bash -s <ARGUMENTS>
    #bash <(curl -s https://gitlab.com/DarwinJS/quick-config/raw/master/macconfig.sh) <ARGUMENTS>

set -eo pipefail #fail fast with exit codes

echo -e "\n*******************"
echo "PHASE: Introduction"
echo " version: $MYVERSION"
echo " Setups up a clean Mac to run GitLab and a Kubernetes cluster in Docker"
echo "Arguments used: $*"
echo -e "*******************\n"

echo "This script utitlizes the 'Do-nothing script' gradual automation methodology articulated here:"
echo "   https://blog.danslimmon.com/2019/07/15/do-nothing-scripting-the-key-to-gradual-automation/"
echo "Consequently it may pause and ask you do to steps manually that aren't yet automated."
echo " "
echo "This script is *mostly* idempotent - which means if it terminates abnormally, once you fix the"
echo "problem that cause the termination, you should be able to re-run it because it will skip stuff"
echo "it did on previous runs."

echo -e "\n*******************"
echo "Checking For Package Manager"
echo -e "*******************\n"

if [[ -z "$(command -v brew)" ]]; then
  echo 'Installing brew and git... '
  URL_BREW='https://raw.githubusercontent.com/Homebrew/install/master/install'
  echo | /usr/bin/ruby -e "$(curl -fsSL $URL_BREW)" > /dev/null
  if [ $? -eq 0 ]; then echo 'OK'; else echo 'NG'; fi
fi

function ifcmdmissing-instpkg () {
#Takes a list of Command / Package Pairs
#If command is not on path, installs the package
for cmdpkg in $1 ; do
    IFS=':' read -ra CMDPKGPAIR <<<"${cmdpkg}"
    CMDNAME=${CMDPKGPAIR[0]}
    PKGNAME=${CMDPKGPAIR[1]}
    echo "If command ${CMDNAME} is missing from the path, the package ${PKGNAME} will be installed..."
    if [[ -n "$(command -v ${CMDNAME})" ]]; then
        echo "  '${CMDNAME}' command already present"
    else
        echo "  Missing command '${CMDNAME}'"
        echo "  Installing package '${PKGNAME}' to resolve missing command."
        if brew info ${PKGNAME} >/dev/null 2>&1; then
          brew install ${PKGNAME} --force
        elif brew cask info ${PKGNAME} >/dev/null 2>&1; then
          brew cask install ${PKGNAME} --force
        else
          echo "  '${PKGNAME}' not found as a formulae or cask"
        fi
    fi
done
}

echo -e "\n*******************"
echo "PHASE: Checking Deployment Automation Dependencies"
echo -e "*******************\n"
ifcmdmissing-instpkg "jq:jq"

echo " => Checking for Xcode CLI tools..."
# Only run if the tools are not installed yet
# Seems to be common that path exists, but xcrun does not - so check what we need
if [[ -z "$(command -v xcrun)" ]]; then
  echo "     Xcode CLI tools not found. Installing them..."
  LOCKFILE=/tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
  touch $LOCKFILE;
  PROD=$(softwareupdate -l |
    grep "\*.*Command Line" |
    head -n 1 | awk -F"*" '{print $2}' |
    sed -e 's/^ Label: *//' |
    tr -d '\n')
  softwareupdate -i "$PROD";
  if [[ -n $LOCKFILE ]]; then rm -f $LOCKFILE; fi
else
  echo "     Xcode CLI tools already installed, skipping..."
fi

echo -e "\n*******************"
echo "PHASE: Checking for Docker and kubectl Utilities..."
echo -e "*******************\n"

ifcmdmissing-instpkg "kubectl:kubernetes-cli docker:docker docker-compose:docker-compose"

if [[ "$(docker-compose --help 2>&1)" == *"Abort trap: 6"* ]]; then
  #Don't know exactly why this is necessary, but it got rid of "Abort trap: 6" as the only
  #output of docker-compose.  See: https://github.com/Homebrew/homebrew-core/issues/45687
  echo "     Fixing docker-compose..."
  brew reinstall -s docker-compose
fi

if [[ "$(uname)" == "Darwin" ]]; then
  echo "  => Completing Mac Docker Install"
  ifcmdmissing-instpkg "VBoxManage:virtualbox docker-machine:docker-machine"
fi

echo "You must manually install Docker Desktop for Mac as there is no brew installer"
echo "KinD does NOT work with Docker Toolbox  (docker, docker-machine, docker-compose)"
echo "Follow these instructions to install Docker Desktop: https://docs.docker.com/docker-for-mac/install/"
read -n 1 -s -r -p "Press any key to open website"
open https://docs.docker.com/docker-for-mac/install/
read -n 1 -s -r -p "When install is complete, press any key to continue..."

# if [[ "$(docker-machine inspect 2>&1 | tr -d '"')" == *"no default machine exists"* ]]; then
#   echo "     Creating default docker machine for Mac docker config"
#   docker-machine create --driver virtualbox default
# fi

# if [[ ! "$(docker-machine status)" == "Running" ]]; then
#   echo "     Starting default docker machine for Mac docker config"
#   docker-machine start
#   eval "$(docker-machine env default)""
# fi

# if [[ "$(docker ps)" == *"Cannot connect to the Docker daemon"* ]]; then
#   echo "     Trying to fix docker..."
#   docker-machine restart
#   eval "$(docker-machine env default)""
# fi

echo -e "\n*******************"
echo "PHASE: Setting up go lang for use with KinD..."
echo -e "*******************\n"

if ! grep -q "GOPATH" ~/.bash_profile; then
cat << 'ENDOFSCRIPT' >> ~/.bash_profile

export GOPATH="${HOME}/go"
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"
ENDOFSCRIPT
fi

export GOPATH="${HOME}/go"
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"
test -d "${GOPATH}" || mkdir "${GOPATH}"
test -d "${GOPATH}/src/github.com" || mkdir -p "${GOPATH}/src/github.com"
ifcmdmissing-instpkg "go:go"

echo "  => setting up KinD..."
if [[ -z "$(command -v kind)" ]]; then
  LATESTKINDVERSION=$(curl https://api.github.com/repos/kubernetes-sigs/kind/tags | jq '.[].name' | sort | tail -n1 | tr -d '"')
  #echo "Installing Kind ${LATESTKINDVERSION} using command GO111MODULE="on" go get -u sigs.k8s.io/kind@${LATESTKINDVERSION}"
  #GO111MODULE="on" go get -u sigs.k8s.io/kind@${LATESTKINDVERSION}
  GO111MODULE="on" go get -u sigs.k8s.io/kind@master
fi

echo -e "\n*****************************"
echo "Virtualization Setup Complete"
echo -e "*****************************\n"