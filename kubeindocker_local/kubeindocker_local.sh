
echo "This has quite a ways to go yet, exiting..."
exit

echo "Using Helm 2.x Rather than 3.x"

brew install helm@2
echo 'export PATH="/usr/local/opt/helm@2/bin:$PATH"' >> ~/.bash_profile
export PATH="/usr/local/opt/helm@2/bin:$PATH"

echo
echo "*******************"
echo "PHASE: Using KIND (Kubernetes in Docker) to create a Kubernetes cluster.  More info: (https://kind.sigs.k8s.io/docs/user/quick-start/)"
kind create cluster --config kindcluster.yml #As of 2019-12-27 Gitlab Helm Charts can not use Kubernetes 1.16 or higher
#export KUBECONFIG="$(kind get kubeconfig-path)"

#Kind ingress docs: https://kind.sigs.k8s.io/docs/user/ingress/ (includes info on stuff already in kindcluster.yml)

echo "Setting up RBAC for GitLab..."
kubectl create -f https://gitlab.com/gitlab-org/charts/gitlab/raw/master/doc/installation/examples/rbac-config.yaml

echo "Setting up Tiller (must be using helm 2.x - not 3.x)..."
helm init --upgrade --service-account tiller

helm repo add gitlab https://charts.gitlab.io/

helm upgrade --install gitlab gitlab/gitlab \
  --set certmanager-issuer.email=dsanoy@gitlab.com \
  --set global.edition=ce

echo "Here is the admin password:"
kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo

echo "Building Ingresses for 80 and 443 to KinD Cluster"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
kubectl patch deployments -n ingress-nginx nginx-ingress-controller -p '{"spec":{"template":{"spec":{"containers":[{"name":"nginx-ingress-controller","ports":[{"containerPort":80,"hostPort":80},{"containerPort":443,"hostPort":443}]}],"nodeSelector":{"ingress-ready":"true"},"tolerations":[{"key":"node-role.kubernetes.io/master","operator":"Equal","effect":"NoSchedule"}]}}}}' 

echo "Ingress can be tested by deploying foo bar ping application 'kubectl create -f test-kind-integress.yml'"



echo "     Creating a key for GitLab to connect..."
kubectl apply -f gitlab-kube-key.yml

echo "     Retrieving Kubernetes Certificate to clipboard"
kubectl config view --raw -o=jsonpath='{.clusters[0].cluster.certificate-authority-data}' | base64 --decode | pbcopy
read -n 1 -s -r -p "Paste the Certificate into GitLabs Kubernetes config and then **Press any key to continue**"

SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
echo $TOKEN | pbcopy
read -n 1 -s -r -p "Paste the Certificate into GitLabs Kubernetes config and then **Press any key to continue**"

cat <<"END_OF_FINAL_INSTRUCTIONS"

For Mac, Your local ip address will be the ip address of the docker machine used to support docker.
Find it's ip address with this command at the console:
docker-machine ls default

IF YOU WILL BE CONNECTING THIS KUBERNETES CLUSTER TO A LOCAL GITLAB INSTALL: 

In GitLab admin, enable local network requests:
Gitlab Admin => Settings => Network => Outbound requests => Allow requests to the local network from web hooks and services (checked)

#get docker network ip and port of kube control plane container
docker network inspect bridge
Use internal ip, e.g. 
docker ps
Use internal port, e.g. 6443
e.g. final: https://172.17.0.2:6443

END_OF_FINAL_INSTRUCTIONS