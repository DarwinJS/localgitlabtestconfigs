
## These scripts implement local test configurations.

### Docker Configuration

These scripts use Docker, not "Docker Desktop for Mac" because
 - Docker Desktop for Mac is hard to install by script.
 - standard docker it is lighter weight
 - most of the scripts / concepts and skills are more transferable to LAMP docker stacks and docker on local Linux Distros

Docker on Mac is a little fussier to setup and use because it has to use "docker-machine" to run docker in virtual box VMs (because the Mac uses a BSD variant kernel - not linux)

### Steps to Configure Mac
Tested on: Catalina 

This script is *mostly* idempotent - which means if it terminates abnormally, once you fix the problem that cause the termination, you should be able to re-run it because it will skip stuff it did on previous runs.

```
chmod +x osx-localvirtualtooling.sh
./osx-localvirtualtooling.sh
```

### Steps to Configure Linux (debian variants)
Tested on: LinuxMint 

Under construction

# Configurations

## Docker on Mac IP Addresses
Docker on Mac uses docker machine.  Containers running in docker will be at a local virtual machine address instead of 127.0.0.1.
To find the local virtual box machine address where your docker containers are, run `docker-machine env default` and examine the output for the ip address on the same line as "DOCKER_HOST"

To use general docker commands, run:
```
eval "$(docker env default)"
docker ps
```

## docker-compose: GitLab on 3 Containers w/ Persisted Local Data

The sameersbn repo is not maintained by GitLab.

IMPORTANT: You can force the version of GitLab (and associated containers) by retreiving the docker-compose.yml associated with a specific tag in this repo: https://github.com/sameersbn/docker-gitlab/tags

```
cd gitlab_local
chmod +x gitlab_local.sh
./gitlab_local.sh
```

## docker-compoose: Jenkins Container w/ Persisted Local Data

The binami repo is not maintained by GitLab.

IMPORTANT: You can force the version of Jenkins an the distro by retreiving the docker-compose.yml associated with a specific tag in this repo: https://github.com/bitnami/bitnami-docker-jenkins/tags

```
cd jenkins_local
chmod +x jenkins_local.sh
./jenkins_local.sh
```

## Kubernetes in Docker (KinD): Jenkins Container w/ Persisted Local Data

Kubernetes in Docker is a part of the Kubernetes project.

THIS IS NOT READY YET

```
cd kubeindocker_local
chmod +x kubeindocker_local.sh
./kubeindocker_local.sh
```