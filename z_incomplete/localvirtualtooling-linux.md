# Docker Installed
# Docker Compose Install
# Ubuntu
apt install docker-compose
wget https://raw.githubusercontent.com/sameersbn/docker-gitlab/master/docker-compose.yml
docker-compose up

docker network connect bridge localgitlabwithkube_gitlab_1

From: https://medium.com/better-programming/using-a-k3s-kubernetes-cluster-for-your-gitlab-project-b0b035c291a9

# Kind Cluster
#Install GO and make sure it is on the path

#Install kubectl Linux:
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
#OR
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl


# Install Kind on Windows
Invoke-WebRequest https://github.com/kubernetes-sigs/kind/releases/download/v0.5.1/kind-windows-amd64 -outfile 
Move-Item .\kind-windows-amd64.exe c:\go\bin\kind.exe

# Create Kind Cluster
# Using Kind: https://kind.sigs.k8s.io/docs/user/quick-start/
kind create cluster --config .\gitlab-kind-cluster.yml

#The about create process outputs the appropriate variable setting string for powershell
export KUBECONFIG="$(kind get kubeconfig-path)"

https://medium.com/better-programming/using-a-k3s-kubernetes-cluster-for-your-gitlab-project-b0b035c291a9

kubectl apply -f gitlab-kube-key.yml

#Get Certificate
#Windows
[System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($(kubectl config view --raw -o=jsonpath='{.clusters[0].cluster.certificate-authority-data}'))) | clip

#Linux:
kubectl config view --raw \
-o=jsonpath='{.clusters[0].cluster.certificate-authority-data}' \
| base64 --decode

#Linux
SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
echo $TOKEN

#windows
kubectl -n kube-system get secret | sls gitlab-admin
$SECRET=
[System.Text.Encoding]::ASCII.GetString([System.Convert]::FromBase64String($(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}'))) | clip

Gitlab Admin => Settings => Network => Outbound requests => Allow requests to the local network from web hooks and services (checked)

#Start Rails in Gitlab Container
localgitlabwithkube_gitlab_1
su - git
cd gitlab
bundle exec rails c production
user = User.where(id: 1).first
user.password = '5iveL!fe'
user.save


#get docker network ip and port of kube control plane container
docker network inspect bridge
Use internal ip, e.g. 
docker ps
Use internal port, e.g. 6443
e.g. final: https://172.17.0.2:6443

-----BEGIN CERTIFICATE-----
MIICyDCCAbCgAwIBAgIBADANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDEwprdWJl
cm5ldGVzMB4XDTE5MDkzMDA5MjUyMVoXDTI5MDkyNzA5MjUyMVowFTETMBEGA1UE
AxMKa3ViZXJuZXRlczCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAPUk
xIQPUMi8scNRxEk6/W1MQrE1Dr80bKgw+f4INVbhrZuaElaQvX8NeuQHMFhI94e3
ZoRNLyFyBOmE/koQsMzw4RXdUcgFDwb0escZac6WPMs78rp8F3bl1O49I8yi0nFo
s+RKja6DsdMtDkGjHZer44OzYpQ0jNnQuqMjTz8Rjx+HZWXtz0qQo4IvHhW9eCeD
npfqPpDc7z9Nxz0pCO97QDElNXGgvl6ImN81BHT4JJnDpd858dLZzuFhtVJuFDR9
tl6tDKGr4dX/xTxjEQ5lJN1XDJEnYZn2CVYje75WD8HKeaW8Fhxk02TU4KEbEjap
QBGrsX1bH5hEBDoTHoECAwEAAaMjMCEwDgYDVR0PAQH/BAQDAgKkMA8GA1UdEwEB
/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBABBcd48TLH25E7IKw/BX3ppfV97P
rYrfTSEm46EBw6n8l/kZZa6FPM+S9emhtaIsD/mL2jIkMpM1jpb9bNTwHKk+IWnT
ztT1Ze2fMFo/pxsfwUnoCMusfXPFm7t7u65zGazXDhwbmUvhDFPyk90laCc+PzSu
MKFkiE809m+6KS09vFn/bqB2JAmuO/m7xmHJSiGLCIyhEZ45lzN9RMiTXSfz7xux
4jNAG6Zcwv6xTI4XoejIutKiK0Ug1kwaueqaGXF7uYHaORAK4gFSOBQbXWTw3BWK
w7dE70QcMdPvEH0krQNHnQG7YtXYos64jNZe1R0Gs+8MlWn7jrghUZ6FRMk=
-----END CERTIFICATE-----

eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJnaXRsYWItYWRtaW4tdG9rZW4tYnE4YzgiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZ2l0bGFiLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiNTc5Zjk2MzYtM2NhMC00YWZhLTg2NzAtMTkwNGY1YWM2NTU3Iiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmdpdGxhYi1hZG1pbiJ9.SFgPp_2H4xMi09VrO1C5RzsaH9Z9ZmAFpY9qKI-gU_A_NM5xotTKZqnEn2vnAPGDX3I7ca_-GSsV6I37Hh18GWNci3mP-Dp_XlZVUJU7ANFZY0X7oKq_uQt4-KPDyjS2F7UHrEZOLxJHzkdGIb_lhSzfLLN5f5Bx0vXSViRtgEx4NMzj5nRv8uEI8_B74IRN7bNUcTntWja_9XSKelr4wvi_trUqq_4vg5PgHTFoFEH9qKQZebPFhBKLV1ZRnSgz2G3P-JASfPbjvxbMEU0sLXWcLVz7PhTl2iN-sAgs81KxHr2NV9kikfmySME3Zfa04pNsXxtw0FNda3l8b1m5lQ





